package com.gitlab.gerhard_piette.defect_2;

/**
 * In case of a bad offset.
 */
public class DefectOffset extends Exception {

	/**
	 * The offset limit.
	 */
	public int limit;

	/**
	 * The bad offset.
	 */
	public int offset;

	public DefectOffset() {
	}

	public DefectOffset(int limit, int offset) {
		this.limit = limit;
		this.offset = offset;
	}

	public String toString() {
		var ret = "(limit:" + limit + " offset:" + offset + ")";
		return ret;
	}
}
